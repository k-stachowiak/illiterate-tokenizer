/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILTOK_INTERNAL_H
#define ILTOK_INTERNAL_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Internal API
// ============

// Internal error handling
// -----------------------

#define INTERNAL_ERROR(CAUSE) do {                                                     \
    fprintf(stderr, "Internal error at %s:%d, cause:%s\n", __FILE__, __LINE__, CAUSE); \
    exit(1);                                                                           \
} while (0)

#define ARRAY_APPEND(MACRO_ARRAY, MACRO_ELEMENT)                        \
    do {                                                                \
        if ((MACRO_ARRAY).cap == 0) {                                   \
            (MACRO_ARRAY).data = malloc(sizeof(*((MACRO_ARRAY).data))); \
            if (!(MACRO_ARRAY).data) {                                  \
                fprintf(stderr, "Not enough memory.");                  \
                exit(2);                                                \
            }                                                           \
            (MACRO_ARRAY).cap = 1;                                      \
        } else if ((MACRO_ARRAY).cap == (MACRO_ARRAY).size) {           \
            void *_temp_;                                               \
            (MACRO_ARRAY).cap *= 2;                                     \
            _temp_ = realloc(                                           \
                (MACRO_ARRAY).data,                                     \
                (MACRO_ARRAY).cap * sizeof(*((MACRO_ARRAY).data)));     \
            if (!_temp_) {                                              \
                fprintf(stderr, "Not enough memory.");                  \
                exit(2);                                                \
            }                                                           \
            (MACRO_ARRAY).data = _temp_;                                \
        }                                                               \
        (MACRO_ARRAY).data[(MACRO_ARRAY).size++] = (MACRO_ELEMENT);     \
    } while (0)

#define ARRAY_FREE(MACRO_ARRAY)                 \
    do {                                        \
        free((MACRO_ARRAY).data);               \
        (MACRO_ARRAY).data = NULL;              \
        (MACRO_ARRAY).size = 0;                 \
        (MACRO_ARRAY).cap = 0;                  \
    } while(0)

#define STR_TEMP_BUFFER_SIZE 1024 * 10

#define STR_APPEND(TXT, FORMAT, ...)                                    \
    do {                                                                \
        char *_buffer_;                                                 \
        int _old_len_, _new_len_;                                       \
        char *_new_text_;                                               \
        _buffer_ = (char *)malloc(STR_TEMP_BUFFER_SIZE);                \
        if (!_buffer_) {                                                \
            fprintf(stderr, "Not enough memory.");                      \
            exit(2);                                                    \
        }                                                               \
        _new_len_ = sprintf(_buffer_, (FORMAT), ##__VA_ARGS__);         \
        if (_new_len_ >= (STR_TEMP_BUFFER_SIZE) - 1) {                  \
            fprintf(stderr, "Buffer of %d not enough for %d bytes.",    \
                    STR_TEMP_BUFFER_SIZE,                               \
                    _new_len_);                                         \
            exit(2);                                                    \
        }                                                               \
        if (!(TXT)) {                                                   \
            _new_text_ = (char *)malloc(_new_len_ + 1);                 \
            if (!_new_text_) {                                          \
                fprintf(stderr, "Not enough memory.");                  \
                exit(2);                                                \
            }                                                           \
            memcpy(_new_text_, _buffer_, _new_len_ + 1);                \
        } else {                                                        \
            _old_len_ = strlen((TXT));                                  \
            _new_text_ = (char *)malloc(_old_len_ + _new_len_ + 1);     \
            if (!_new_text_) {                                          \
                fprintf(stderr, "Not enough memory.");                  \
                exit(2);                                                \
            }                                                           \
            memcpy(_new_text_, (TXT), _old_len_);                       \
            memcpy(_new_text_ + _old_len_, _buffer_, _new_len_ + 1);    \
            free((TXT));                                                \
        }                                                               \
        (TXT) = _new_text_;                                             \
        free(_buffer_);                                                 \
    } while(0)

static inline char *str_copy(char *x)
{
    int len;
    char *result;
    if (!x) {
        return NULL;
    }
    len = strlen(x);
    result = malloc(len + 1);
    if (!result) {
        fprintf(stderr, "Not enough memory.");
        exit(2);
    }
    memcpy(result, x, len + 1);
    return result;
}

static inline char *str_copy_range(char *first, char *last)
{
    int size = last - first;
    char *result = malloc(size + 1);
    if (!result) {
        fprintf(stderr, "Not enough memory.");
        exit(2);
    }
    memcpy(result, first, size);
    result[size] = '\0';
    return result;
}

#endif
