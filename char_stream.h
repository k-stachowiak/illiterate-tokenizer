/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILTOK_CHAR_STREAM_H
#define ILTOK_CHAR_STREAM_H

#include <stdbool.h>
#include <stdio.h>

// Abstract character stream
// =========================

struct TokCharStream {
    void *state;
    void (*deinit)(void *state);
    bool (*has_next)(void *state);
    int  (*peek_next)(void *state);
    int  (*get_next)(void *state);
};

// Constructors
// ------------

void tok_cs_init_string(struct TokCharStream *cs, char *string);
void tok_cs_init_file(struct TokCharStream *cs, FILE *file);

// Common operations
// -----------------

void tok_cs_deinit(struct TokCharStream *cs);
bool tok_cs_has_next(struct TokCharStream *cs);
int tok_cs_peek_next(struct TokCharStream *cs);
int tok_cs_get_next(struct TokCharStream *cs);

void tok_cs_skip_if(struct TokCharStream *cs, int (*predicate)(int));

// Test code
// ---------

int tok_cs_test(void);

#endif
