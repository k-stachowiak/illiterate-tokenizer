/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"
#include "buffered_char_stream.h"

#include <stdlib.h>
#include <string.h>

// Public API implementation
// =========================

void tok_bcs_init(struct TokBufferedCharacterStream *bcs, struct TokCharStream *cs)
{
    bcs->cs = cs;
    bcs->buffer.data = NULL;
    bcs->buffer.size = bcs->buffer.cap = 0;
    bcs->buffer_end = 0;
    bcs->source_offset = 0;
}

void tok_bcs_deinit(struct TokBufferedCharacterStream *bcs)
{
    bcs->source_offset = 0;
    bcs->buffer_end = 0;
    ARRAY_FREE(bcs->buffer);
    bcs->cs = NULL;
}

bool tok_bcs_has_next(struct TokBufferedCharacterStream *bcs)
{
    return
        bcs->buffer_end < bcs->buffer.size || // EITHER more chars in buffer,
        tok_cs_has_next(bcs->cs);             // OR more chars in the stream
}

int tok_bcs_peek_next(struct TokBufferedCharacterStream *bcs)
{
    int result;
    if (bcs->buffer_end == bcs->buffer.size) {
        result = tok_cs_peek_next(bcs->cs);
    } else {
        result = bcs->buffer.data[bcs->buffer_end];
    }
    return result;
}

int tok_bcs_get_next(struct TokBufferedCharacterStream *bcs)
{
    int result;

    // 1. Read a character from one of the sources:
    if (bcs->buffer_end == bcs->buffer.size) {
        // 1.a) no data in buffer, read from stream:
        result = tok_cs_get_next(bcs->cs);
        ARRAY_APPEND(bcs->buffer, result);
    } else {
        // 1.b) data still in buffer, read from there:
        result = bcs->buffer.data[bcs->buffer_end];
    }

    // 2. Regardless of the case above, advance read:
    ++bcs->buffer_end;
    ++bcs->source_offset;

    // 3. Return the acquired character:
    return result;
}

void tok_bcs_reset(struct TokBufferedCharacterStream *bcs)
{
    bcs->source_offset -= bcs->buffer_end;
    bcs->buffer_end = 0;
}

void tok_bcs_reset_n(struct TokBufferedCharacterStream *bcs, int n)
{
    if (n <= bcs->buffer_end ) {
        bcs->buffer_end -= n;
        bcs->source_offset -= n;
    } else {
        INTERNAL_ERROR("Attempted back tracking with nothing buffered");
    }
}

void tok_bcs_commit(struct TokBufferedCharacterStream *bcs)
{
    if (!bcs->buffer.data)
        return;

    memmove(bcs->buffer.data,
            bcs->buffer.data + bcs->buffer_end,
            bcs->buffer.size - bcs->buffer_end);
    bcs->buffer.size -= bcs->buffer_end;
    bcs->buffer_end  -= bcs->buffer_end;
}

char *tok_bcs_copy_buffered(struct TokBufferedCharacterStream *bcs)
{
    return str_copy_range(bcs->buffer.data,
                          bcs->buffer.data + bcs->buffer_end);
}

// Test Code
// =========

static int bcs_test_generic(char *case_description, bool result)
{
    if (result) {
        return 0;
    } else {
        printf("Failed test case: %s\n", case_description);
        return 1;
    }
}

static int bcs_test_just_read(void)
{
    int errors = 0;
    struct TokCharStream cs;
    struct TokBufferedCharacterStream bcs;

    char *input = "abc";

    tok_cs_init_string(&cs, input);
    tok_bcs_init(&bcs, &cs);

    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   0);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek first",     tok_bcs_peek_next(&bcs) == 'a');
    errors += bcs_test_generic("Properly get first",      tok_bcs_get_next(&bcs) ==  'a');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek second",    tok_bcs_peek_next(&bcs) == 'b');
    errors += bcs_test_generic("Properly get second",     tok_bcs_get_next(&bcs) ==  'b');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek third",     tok_bcs_peek_next(&bcs) == 'c');
    errors += bcs_test_generic("Properly get third",      tok_bcs_get_next(&bcs) ==  'c');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);

    errors += bcs_test_generic("No longer has next",      tok_bcs_has_next(&bcs) ==  false);

    tok_bcs_deinit(&bcs);
    tok_cs_deinit(&cs);

    return errors;
}

static int bcs_test_read_commit(void)
{
    int errors = 0;
    struct TokCharStream cs;
    struct TokBufferedCharacterStream bcs;

    char *input = "abc";

    tok_cs_init_string(&cs, input);
    tok_bcs_init(&bcs, &cs);

    bcs_test_generic("Check source offset",     bcs.source_offset ==   0);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek first",     tok_bcs_peek_next(&bcs) == 'a');
    errors += bcs_test_generic("Properly get first",      tok_bcs_get_next(&bcs) ==  'a');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);
    tok_bcs_commit(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek second",    tok_bcs_peek_next(&bcs) == 'b');
    errors += bcs_test_generic("Properly get second",     tok_bcs_get_next(&bcs) ==  'b');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);
    tok_bcs_commit(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek third",     tok_bcs_peek_next(&bcs) == 'c');
    errors += bcs_test_generic("Properly get third",      tok_bcs_get_next(&bcs) ==  'c');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);
    tok_bcs_commit(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);

    errors += bcs_test_generic("No longer has next",      tok_bcs_has_next(&bcs) ==  false);

    tok_bcs_deinit(&bcs);
    tok_cs_deinit(&cs);

    return errors;
}

static int bcs_test_read_reset_commit(void)
{
    int errors = 0;
    struct TokCharStream cs;
    struct TokBufferedCharacterStream bcs;

    char *input = "abc";

    tok_cs_init_string(&cs, input);
    tok_bcs_init(&bcs, &cs);

    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   0);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek first",     tok_bcs_peek_next(&bcs) == 'a');
    errors += bcs_test_generic("Properly get first",      tok_bcs_get_next(&bcs) ==  'a');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);
    tok_bcs_reset(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   0);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek first",     tok_bcs_peek_next(&bcs) == 'a');
    errors += bcs_test_generic("Properly get first",      tok_bcs_get_next(&bcs) ==  'a');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);
    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek second",    tok_bcs_peek_next(&bcs) == 'b');
    errors += bcs_test_generic("Properly get second",     tok_bcs_get_next(&bcs) ==  'b');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);
    tok_bcs_reset(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   0);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek first",     tok_bcs_peek_next(&bcs) == 'a');
    errors += bcs_test_generic("Properly get first",      tok_bcs_get_next(&bcs) ==  'a');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);
    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek second",    tok_bcs_peek_next(&bcs) == 'b');
    errors += bcs_test_generic("Properly get second",     tok_bcs_get_next(&bcs) ==  'b');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);
    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek third",     tok_bcs_peek_next(&bcs) == 'c');
    errors += bcs_test_generic("Properly get third",      tok_bcs_get_next(&bcs) ==  'c');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);
    tok_bcs_reset(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   0);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek first",     tok_bcs_peek_next(&bcs) == 'a');
    errors += bcs_test_generic("Properly get first",      tok_bcs_get_next(&bcs) ==  'a');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);
    tok_bcs_commit(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek second",    tok_bcs_peek_next(&bcs) == 'b');
    errors += bcs_test_generic("Properly get second",     tok_bcs_get_next(&bcs) ==  'b');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);
    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek third",     tok_bcs_peek_next(&bcs) == 'c');
    errors += bcs_test_generic("Properly get third",      tok_bcs_get_next(&bcs) ==  'c');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);
    tok_bcs_reset(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   1);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek second",    tok_bcs_peek_next(&bcs) == 'b');
    errors += bcs_test_generic("Properly get second",     tok_bcs_get_next(&bcs) ==  'b');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);
    tok_bcs_commit(&bcs);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek third",     tok_bcs_peek_next(&bcs) == 'c');
    errors += bcs_test_generic("Properly get third",      tok_bcs_get_next(&bcs) ==  'c');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);
    tok_bcs_reset_n(&bcs, 1);
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   2);

    errors += bcs_test_generic("Has next before reading", tok_bcs_has_next(&bcs) ==  true);
    errors += bcs_test_generic("Properly peek third",     tok_bcs_peek_next(&bcs) == 'c');
    errors += bcs_test_generic("Properly get third",      tok_bcs_get_next(&bcs) ==  'c');
    errors += bcs_test_generic("Check source offset",     bcs.source_offset ==   3);

    errors += bcs_test_generic("No longer has next",      tok_bcs_has_next(&bcs) ==  false);

    tok_bcs_deinit(&bcs);
    tok_cs_deinit(&cs);

    return errors;
}

int tok_bcs_test(void)
{
    return
        bcs_test_just_read() +
        bcs_test_read_commit() +
        bcs_test_read_reset_commit();
}
