/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "internal.h"
#include "token_stream.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

// Helper algorithms:
// ==================

static bool validate_hard_token(char *token)
{
    // For the sake of the tokenizer's operation, we don't allow white spaces
    // inside hard tokens.

    while (*token) {
        if (isspace(*token)) {
            return false;
        }
        ++token;
    }

    return true;
}

static bool validate_delimiter(char *delimiter)
{
    // For the sake of the tokenizer's operation, we don't allow white spaces
    // inside delimiters.

    while (*delimiter) {
        if (isspace(*delimiter)) {
            return false;
        }
        ++delimiter;
    }

    return true;
}

static int compare_hard_tokens(const void *x, const void *y) {
    const char **str_x = (const char **)x;
    const char **str_y = (const char **)y;
    int len_x = strlen(*str_x);
    int len_y = strlen(*str_y);
    if (len_x != len_y) {
        return len_y - len_x;
    } else {
        return strcmp(*str_x, *str_y);
    }
}

static void sort_hard_tokens(struct TokenStream *ts) {
    qsort(ts->hard_tokens.data,
          ts->hard_tokens.size,
          sizeof(*ts->hard_tokens.data),
          compare_hard_tokens);
}

static void skip_white_spaces(struct TokenStream *ts)
{
    while (tok_bcs_has_next(&ts->bcs) && isspace(tok_bcs_peek_next(&ts->bcs))) {
        (void)tok_bcs_get_next(&ts->bcs);
    }
    tok_bcs_commit(&ts->bcs);
}

// Token reading algorithms
// ========================

static char *try_peeking_hard_token(struct TokenStream *ts)
{
    char **hard_tokens_first = ts->hard_tokens.data;
    char **hard_tokens_last = hard_tokens_first + ts->hard_tokens.size;

    while (hard_tokens_first != hard_tokens_last) {

        // 1. Reset compared reading locations:
        char *hard_token_it = *hard_tokens_first;
        bool match = true;
        tok_bcs_reset(&ts->bcs);

        // 2. Iterate over the current hard token character by character:
        while (*hard_token_it) {

            int next;

            // 2.1. No more characters in the buffer - hard token not matched:
            if (!tok_bcs_has_next(&ts->bcs)) {
                match = false;
                break;
            }

            // 2.2. Take the next source character...
            next = tok_bcs_get_next(&ts->bcs);
            if (next != *hard_token_it) {
                match = false;
                break;
            }

            // 2.3. Advance the hard token read location:
            ++hard_token_it;
        }

        // 3. Check if hard token matched:
        if (match) {
            char *result = tok_bcs_copy_buffered(&ts->bcs);
            return result;
        }

        // 4. Advance to the next hard token:
        ++hard_tokens_first;
    }

    return NULL;
}

static char *try_reading_hard_token(struct TokenStream *ts)
{
    char *result = try_peeking_hard_token(ts);
    if (result) {
        tok_bcs_commit(&ts->bcs);
    }
    return result;
}

static char *try_peeking_delimiter(struct TokenStream *ts)
{
    char ***delimited_pairs_first = ts->delimiter_pairs.data;
    char ***delimited_pairs_last = delimited_pairs_first + ts->delimiter_pairs.size;

    while (delimited_pairs_first != delimited_pairs_last) {

        // 1. Initialize delimiter iteration:
        char *delimiter_it = (*delimited_pairs_first)[0];
        bool match = true;
        tok_bcs_reset(&ts->bcs);

        // 2. Try matching subsequent delimiter characters:
        while (*delimiter_it) {

            int next;

            // 2.1. No more characters in the buffer - delimiter not matched:
            if (!tok_bcs_has_next(&ts->bcs)) {
                match = false;
                break;
            }

            // 2.2. Read next character and see if matches opening delimiter:
            next = tok_bcs_get_next(&ts->bcs);
            if (next != *delimiter_it) {
                match = false;
                break;
            }

            // 2.3. Next delimiter character:
            ++delimiter_it;
        }

        // 3. Check if delimiter matched:
        if (match) {
            return (*delimited_pairs_first)[1];
        }

        // 4. Advance to next delimiter pair:
        ++delimited_pairs_first;
    }

    return NULL;
}

static char *try_reading_delimiter(struct TokenStream *ts)
{
    char *result = try_peeking_delimiter(ts);
    if (result) {
        tok_bcs_commit(&ts->bcs);
    }
    return result;
}

static char *read_until_delimiter(struct TokenStream *ts, char *delimiter)
{
    char *delimiter_it = delimiter;
    char *result;

    // 1. Try matching delimiter, while buffering whatever comes in:
    while (*delimiter_it) {

        int next;

        // 1.1. React to premature end of stream:
        if (!tok_bcs_has_next(&ts->bcs)) {
            ARRAY_APPEND(
                ts->error_stack,
                str_copy("End of input encountered while expecting closing delimiter"));
            return NULL;
        }

        // 1.2. Buffer in the next character:
        next = tok_bcs_get_next(&ts->bcs);

        // 1.3. Try to match against the current delimiter character:
        if (next == *delimiter_it) {
            // 1.3.a) Matched, hope to match next delimiter character:
            ++delimiter_it;
        } else {
            // 1.3.b) Didn't match, reset the delimiter character:
            int matched_len = delimiter_it - delimiter;
            tok_bcs_reset_n(&ts->bcs, matched_len);
            delimiter_it = delimiter;
        }
    }

    // 2. Return whatever was matched:
    result = tok_bcs_copy_buffered(&ts->bcs);
    tok_bcs_commit(&ts->bcs);
    return result;
}

static struct Token read_next_token(struct TokenStream* ts)
{
    struct { char *data; int size, cap; } regular = { NULL, 0, 0 };
    size_t offset_before = ts->bcs.source_offset;
    struct Token result;

    // 1. Accumulate characters until one of the following happens:
    //    a) hard token found,
    //    b) delimited token found,
    //    c) white space found.
    while (tok_bcs_has_next(&ts->bcs) &&
           !isspace(tok_bcs_peek_next(&ts->bcs))) {

        int next;
        char *hard;
        char *closing;

        // 1.1. Try reading hard token:
        hard = try_peeking_hard_token(ts);
        if (hard) {
            if (regular.data) {
                // 1.1.a) Irregular found but regular token was also read:
                tok_bcs_reset(&ts->bcs);
                free(hard);
                ARRAY_APPEND(regular, '\0');
                tok_init(&result, regular.data, offset_before);
                return result;
            } else {
                // 1.1.b) Irregular found and no regular was read so far:
                tok_bcs_commit(&ts->bcs);
                skip_white_spaces(ts);
                tok_init(&result, hard, offset_before);
                return result;
            }
        }
        tok_bcs_reset(&ts->bcs);

        // 1.2. Try reading delimited token:
        closing = try_peeking_delimiter(ts);
        if (closing) {
            if (regular.data) {
                // 1.2.a) Delimited found but regular token was also read:
                tok_bcs_reset(&ts->bcs);
                ARRAY_APPEND(regular, '\0');
                tok_init(&result, regular.data, offset_before);
                return result;
            } else {
                // 1.2.b) Delimited found and no regular was read so far:
                char *head, *tail, *total;
                head = tok_bcs_copy_buffered(&ts->bcs);
                tok_bcs_commit(&ts->bcs);
                tail = read_until_delimiter(ts, closing);
                total = NULL;
                STR_APPEND(total, "%s%s", head, tail);
                free(head);
                free(tail);
                skip_white_spaces(ts);
                tok_init(&result, total, offset_before);
                return result;
            }
        }
        tok_bcs_reset(&ts->bcs);

        // 1.3. If we've got here, read next character of the regular token:
        next = tok_bcs_get_next(&ts->bcs);
        ARRAY_APPEND(regular, next);
        tok_bcs_commit(&ts->bcs);
    }

    // 2. Regular token building loop left due to reaching white space or end of
    //    character stream:
    if (regular.data) {
        // 2.a) Regular token was accumulated => return it:
        skip_white_spaces(ts);
        ARRAY_APPEND(regular, '\0');
        tok_init(&result, regular.data, offset_before);
        return result;
    } else {
        // 2.b) Regular token was not accumulated => report error:
        INTERNAL_ERROR("Attempted token reading in invalid state");
    }
}

// Public API implementation
// =========================

// Token type
// ----------

void tok_init(struct Token *token, char *string, size_t source_offset)
{
    token->string = string;
    token->source_offset = source_offset;
}

void tok_init_invalid(struct Token *token)
{
    tok_init(token, NULL, -1);
}

void tok_deinit(struct Token *token)
{
    token->source_offset = -1;
    if (token->string) {
        free(token->string);
        token->string = NULL;
    }
}

bool tok_valid(struct Token *token)
{
    return (bool)token->string;
}

struct Token tok_copy(struct Token *token)
{
    struct Token result;
    tok_init(&result, str_copy(token->string), token->source_offset);
    return result;
}

// Token stream type
// -----------------

void tok_ts_init(struct TokenStream *ts, struct TokCharStream *cs)
{
    // 1. Initialize the structure:
    tok_bcs_init(&ts->bcs, cs);
    ts->hard_tokens.data = NULL;
    ts->hard_tokens.size = ts->hard_tokens.cap = 0;
    ts->delimiter_pairs.data = NULL;
    ts->delimiter_pairs.size = ts->delimiter_pairs.cap = 0;
    tok_init_invalid(&ts->last_token);

    ts->error_stack.data = NULL;
    ts->error_stack.size = ts->error_stack.cap = 0;

    // 2. Drop all the leading white spaces:
    skip_white_spaces(ts);
}

void tok_ts_deinit(struct TokenStream *ts) {
    int i;
    for (i = 0; i != ts->error_stack.size; ++i) {
        free(ts->error_stack.data[i]);
    }
    ARRAY_FREE(ts->error_stack);
    tok_deinit(&ts->last_token);
    for (i = 0; i != ts->hard_tokens.size; ++i) {
        free(ts->hard_tokens.data[i]);
    }
    ARRAY_FREE(ts->hard_tokens);
    for (i = 0; i != ts->delimiter_pairs.size; ++i) {
        free(ts->delimiter_pairs.data[i][0]);
        free(ts->delimiter_pairs.data[i][1]);
        free(ts->delimiter_pairs.data[i]);
    }
    ARRAY_FREE(ts->delimiter_pairs);
    tok_bcs_deinit(&ts->bcs);
}

bool tok_ts_add_hard_tokens(struct TokenStream *ts,
                            char **tokens,
                            int tokens_count)
{
    int i;
    for (i = 0; i != tokens_count; ++i) {
        if (!validate_hard_token(tokens[i])) {
            char *error_message = NULL;
            STR_APPEND(error_message,
                       "Invalid hard token: %s", tokens[i]);
            ARRAY_APPEND(ts->error_stack, error_message);
            return false;
        }
        ARRAY_APPEND(ts->hard_tokens, str_copy(tokens[i]));
    }
    sort_hard_tokens(ts);
    return true;
}

bool tok_ts_add_delimiter_pairs(struct TokenStream *ts,
                                char *(*delimiters)[2],
                                int delimiters_count)
{
    int i;
    for (i = 0; i != delimiters_count; ++i) {
        char **pair;

        if (!validate_delimiter(delimiters[i][0])) {
            char *error_message = NULL;
            STR_APPEND(error_message,
                       "Invalid delimiter: %s", delimiters[i][0]);
            ARRAY_APPEND(ts->error_stack, error_message);
            return false;
        }

        if (!validate_delimiter(delimiters[i][1])) {
            char *error_message = NULL;
            STR_APPEND(error_message,
                       "Invalid delimiter: %s", delimiters[i][1]);
            ARRAY_APPEND(ts->error_stack, error_message);
            return false;
        }

        pair = malloc(2 * sizeof(char *));
        if (!pair) {
            INTERNAL_ERROR("Out of memory");
        }

        pair[0] = str_copy(delimiters[i][0]);
        pair[1] = str_copy(delimiters[i][1]);

        ARRAY_APPEND(ts->delimiter_pairs, pair);
    }
    return true;
}

bool tok_ts_has_next(struct TokenStream *ts)
{
    return tok_valid(&ts->last_token) || tok_bcs_has_next(&ts->bcs);
}

char *tok_ts_peek_next(struct TokenStream *ts)
{
    if (!tok_ts_has_next(ts)) {
        INTERNAL_ERROR("Illegal token stream read");
    }
    if (!tok_valid(&ts->last_token)) {
        ts->last_token = read_next_token(ts);
    }
    return ts->last_token.string;
}

size_t tok_ts_peek_offset(struct TokenStream *ts)
{
    if (!tok_ts_has_next(ts)) {
        INTERNAL_ERROR("Illegal token stream read");
    }
    if (!tok_valid(&ts->last_token)) {
        ts->last_token = read_next_token(ts);
    }
    return ts->last_token.source_offset;
}

struct Token tok_ts_get_next(struct TokenStream *ts)
{
    struct Token result;
    if (!tok_ts_has_next(ts)) {
        INTERNAL_ERROR("Illegal token stream read");
    }
    if (tok_valid(&ts->last_token)) {
        // Note: we're moving ownership of the string buffer between the
        // previous "last token" and the returned value". We then reinitialize
        // the "last token" to the invalid state to indicate the data has been
        // moved away from it.
        result = ts->last_token;
        tok_init_invalid(&ts->last_token);
        return result;
    } else {
        return read_next_token(ts);
    }
}

void tok_ts_drop_next(struct TokenStream *ts)
{
    struct Token temp = tok_ts_get_next(ts);
    tok_deinit(&temp);
}

// Test code
// =========

static int ts_test_generic(char *case_description, bool result)
{
    if (result) {
        return 0;
    } else {
        printf("Failed test case: %s\n", case_description);
        return 1;
    }
}

// Helper algorithms tests
// -----------------------

static int ts_test_hard_token_comparison(char *description,
                                         char *string_1,
                                         char *string_2,
                                         int expected_result)
{
    return ts_test_generic(
        description,
        compare_hard_tokens(&string_1, &string_2) ==
        expected_result);
}

static int ts_test_hard_token_comparisons(void)
{
    return
        ts_test_hard_token_comparison("Equal tokens", "==", "==", 0) +
        ts_test_hard_token_comparison("Different length", "===", "==", -1) +
        ts_test_hard_token_comparison("Different length", "==", "===", 1) +
        ts_test_hard_token_comparison("Different content", "=>", "=", -1) +
        ts_test_hard_token_comparison("Different content", "=", "=>", 1);
}

// Simple test cases
// -----------------

static int ts_test_empty(void)
{
    int errors;

    struct TokCharStream cs;
    struct TokenStream ts;

    tok_cs_init_string(&cs, "");
    tok_ts_init(&ts, &cs);

    errors = ts_test_generic(
               "Empty has no next",
               tok_ts_has_next(&ts) == false);

    tok_ts_deinit(&ts);
    tok_cs_deinit(&cs);

    return errors;
}

// Hard token specific tests
// -------------------------

static int ts_test_hard_token(char *description,
                              char *hard_token,
                              char *input,
                              char *expected)
{
    int errors;

    struct TokCharStream cs;
    struct TokenStream ts;

    char *hard_tokens[] = { hard_token };
    char *actual;

    tok_cs_init_string(&cs, input);
    tok_ts_init(&ts, &cs);

    tok_ts_add_hard_tokens(&ts, hard_tokens, 1);

    actual = try_reading_hard_token(&ts);

    if (expected) {
        errors = ts_test_generic(description, actual && strcmp(expected, actual) == 0);
    } else {
        errors = ts_test_generic(description, actual == NULL);
    }

    free(actual);

    tok_ts_deinit(&ts);
    tok_cs_deinit(&cs);

    return errors;
}

static int ts_test_hard_token_detection(void)
{
    return
        ts_test_hard_token("Detect hard token", "asd", "asd ", "asd") +
        ts_test_hard_token("Detect hard token", "asd", " \t\nasd\t \n", "asd") +
        ts_test_hard_token("Detect lack of hard token", "asd", "as", NULL) +
        ts_test_hard_token("Detect lack of hard token", "asd", "as ", NULL) +
        ts_test_hard_token("Detect lack of hard token", "asd", " as ", NULL);
}

// Delimited token specific tests
// ------------------------------

static int ts_test_opening_delimiter(char *description,
                                     char *opening,
                                     char *closing,
                                     char *input,
                                     char *expected)
{
    int errors;

    struct TokCharStream cs;
    struct TokenStream ts;

    char *delimiter_pairs[][2] = { { opening, closing } };
    char *actual;

    tok_cs_init_string(&cs, input);
    tok_ts_init(&ts, &cs);

    tok_ts_add_delimiter_pairs(&ts, delimiter_pairs, 1);

    actual = try_reading_delimiter(&ts);

    if (expected) {
        errors = ts_test_generic(description, actual && strcmp(expected, actual) == 0);
    } else {
        errors = ts_test_generic(description, actual == NULL);
    }

    tok_ts_deinit(&ts);
    tok_cs_deinit(&cs);

    return errors;
}

static int ts_test_opening_delimiter_detection(void)
{
    return
        ts_test_opening_delimiter("Detect one-char delimiter", "(", ")", "(", ")") +
        ts_test_opening_delimiter("Detect one-char delimiter", "(", ")", "(asd", ")") +
        ts_test_opening_delimiter("Detect one-char delimiter", "(", ")", "( asd", ")") +
        ts_test_opening_delimiter("Detect multi-char delimiter", "<!--", "-->", "<!--", "-->") +
        ts_test_opening_delimiter("Detect multi-char delimiter", "<!--", "-->", "<!--asd", "-->") +
        ts_test_opening_delimiter("Detect multi-char delimiter", "<!--", "-->", "<!-- asd", "-->") +

        ts_test_opening_delimiter("Detect lack of one-char delimiter",
                                  "(", ")", "asd", NULL) +

        ts_test_opening_delimiter("Detect lack of multi-char delimiter",
                                  "<!--", "-->", "asd", NULL);
}

// Token sequence tests
// --------------------

static int ts_test_token_sequence(char *input,
                                  char **expected_tokens,
                                  size_t *expected_offsets,
                                  char **hard_tokens,
                                  char **delimiters)
{
    struct TokCharStream cs;
    struct TokenStream ts;

    char **current_expected_token = expected_tokens;
    size_t *current_expected_offset = expected_offsets;
    char *description = NULL;
    struct Token actual;
    char *failure_reason;

    // 1. Initialize the tested setup:

    // 1.1. Basic setup:
    tok_cs_init_string(&cs, input);
    tok_ts_init(&ts, &cs);

    // 1.2. Optional hard tokens injection:
    if (hard_tokens) {
        char **current_hard_token = hard_tokens;
        int hard_tokens_count = 0;
        while (*current_hard_token++) {
            ++hard_tokens_count;
        }
        tok_ts_add_hard_tokens(&ts, hard_tokens, hard_tokens_count);
    }

    // 1.3. Optional delimiters injection:
    if (delimiters) {
        char **current_delimiter = delimiters;
        int delimiters_count = 0;
        while (*current_delimiter++) {
            ++delimiters_count;
        }
        if (delimiters_count & 1) {
            INTERNAL_ERROR("Odd count of delimiters passed into test case");
        }
        current_delimiter = delimiters;
        while (*current_delimiter) {
            char *pair[] = { *current_delimiter,
                             *(current_delimiter + 1) };
            tok_ts_add_delimiter_pairs(&ts, &pair, 1);
            current_delimiter += 2;
        }
    }

    // 2. Read tokens one by one and check against the expected ones:
    while (tok_ts_has_next(&ts)) {

        // 2.a) Check if we still expect tokens:
        if (!*current_expected_token) {
            failure_reason = "expected token list depleted before comparison end";
            goto fail;
        }

        // 2.b) Read next token and compare against expected:
        actual = tok_ts_get_next(&ts);
        if (!tok_valid(&actual)) {
            failure_reason = "token read failure";
            goto fail;
        }
        if (strcmp(actual.string, *current_expected_token) != 0) {
            failure_reason = "token mismach during comparison";
            goto fail;
        }
        if (actual.source_offset != *current_expected_offset) {
            failure_reason = "offset mismach during comparison: ";
            goto fail;
        }

        // 3.c) Advance to the next expected token:
        tok_deinit(&actual);
        ++current_expected_token;
        ++current_expected_offset;
    }

    // 3. Check if we still expect tokens after depleting the input stream:
    if (*current_expected_token) {
        failure_reason = "expected token list not depleated during comparison";
        goto fail;
    }

    // 4.a) Everything went well:

    // 4.a).1 Clean up:
    tok_ts_deinit(&ts);
    tok_cs_deinit(&cs);

    // 4.a).1 Report success:
    return 0;

fail:
    // 4.b) Failure occured:

    // 4.b)1. Clean up test setup:
    tok_deinit(&actual);
    tok_ts_deinit(&ts);
    tok_cs_deinit(&cs);

    // 4.b)2. Construct an elaborate failure report:
    STR_APPEND(
        description,
        "Failed tokenizing string: \"%s\" into a token sequence : \"",
        input);
    current_expected_token = expected_tokens;
    current_expected_offset = expected_offsets;
    while (*current_expected_token) {
        STR_APPEND(description,
                   "%s@%zu ",
                   *current_expected_token,
                   *current_expected_offset);
        ++current_expected_token;
        ++current_expected_offset;
    }
    STR_APPEND(description, "\", because %s", failure_reason);

    // 4.b)2. Print failure:
    printf("Failed: %s\n", description);
    free(description);

    // 4.b)3. Report failure:
    return 1;
}

static int ts_test_regular_sequence(void)
{
    char *input_1 = " a b c ";
    char *input_2 = "a b c";

    char *tokens[] = { "a", "b", "c", NULL };

    size_t offsets_1[] = { 1, 3, 5 };
    size_t offsets_2[] = { 0, 2, 4 };

    return
        ts_test_token_sequence(input_1, tokens, offsets_1, NULL, NULL) +
        ts_test_token_sequence(input_2, tokens, offsets_2, NULL, NULL);
}

static int ts_test_sequence_with_hard_tokens(void)
{
    char *input_1 = "(2+2)*2";
    char *input_2 = " ( 2 + 2 ) * 2 ";

    char *tokens[] = { "(", "2", "+", "2", ")", "*", "2", NULL };

    size_t offsets_1[] = { 0, 1, 2, 3, 4, 5, 6 };
    size_t offsets_2[] = { 1, 3, 5, 7, 9, 11, 13 };

    char *hard[] = { "(", "+", ")", "*", NULL };

    return
        ts_test_token_sequence(input_1, tokens, offsets_1, hard, NULL) +
        ts_test_token_sequence(input_2, tokens, offsets_2, hard, NULL);
}

static int ts_test_sequence_with_delimited_tokens(void)
{
    char *input_11 = "\"\"''";
    char *input_12 = "\"\" ''";
    char *input_21 = "\"asd\"'f'";
    char *input_22 = " \"asd\" 'f' ";

    char *expected_1[] = { "\"\"", "''", NULL };
    char *expected_2[] = { "\"asd\"", "'f'", NULL };

    size_t offsets_11[] = { 0, 2 };
    size_t offsets_12[] = { 0, 3 };
    size_t offsets_21[] = { 0, 5 };
    size_t offsets_22[] = { 1, 7 };

    char *delimiters[] = { "\"", "\"", "'", "'", NULL };

    return
        ts_test_token_sequence(input_11, expected_1, offsets_11, NULL, delimiters) +
        ts_test_token_sequence(input_12, expected_1, offsets_12, NULL, delimiters) +
        ts_test_token_sequence(input_21, expected_2, offsets_21, NULL, delimiters) +
        ts_test_token_sequence(input_22, expected_2, offsets_22, NULL, delimiters);
}

static int ts_test_four_token_sequence(char **hard,
                                       char **delimiters,
                                       char *a,
                                       char *b,
                                       char *c,
                                       char *d)
{
    int errors;

    char *expected[] = { a, b, c, d, NULL };
    size_t offsets[] = { 0, 4, 8, 12 };
    char *input = NULL;

    STR_APPEND(input, "%s %s %s %s", a, b, c, d);
    errors = ts_test_token_sequence(input, expected, offsets, hard, delimiters);
    free(input);

    return errors;
}

static int ts_test_sequence_of_mixed_tokens(void)
{
    int errors = 0;

    char *tokens[] = {
        "asd", // regular
        "+++", // hard
        "'1'"  // delimited
    };
    char *hard[] = { "+++", NULL};
    char *delimiters[] = { "'", "'", NULL};

    int i, j, k;

    for (i = 0; i != 3; ++i) {
        for (j = 0; j != 3; ++j) {
            for (k = 0; k != 3; ++k) {
                errors +=
                    ts_test_four_token_sequence(
                        hard, delimiters,
                        tokens[i], tokens[j], tokens[k], tokens[i]);
            }
        }
    }

    return errors;
}

// Test entry point
// ----------------

int tok_ts_test(void)
{
    return
        ts_test_empty() +
        ts_test_hard_token_comparisons() +
        ts_test_hard_token_detection() +
        ts_test_opening_delimiter_detection() +
        ts_test_regular_sequence() +
        ts_test_sequence_with_hard_tokens() +
        ts_test_sequence_with_delimited_tokens() +
        ts_test_sequence_of_mixed_tokens();
}
