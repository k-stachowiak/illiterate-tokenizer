/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Illiterate Tokenizer is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "buffered_char_stream.h"
#include "char_stream.h"
#include "token_stream.h"

#include <stdio.h>

int main(void) {
    int errors =
        tok_bcs_test() +
        tok_cs_test() +
        tok_ts_test();
    printf("Performed test. Error count: %d\n", errors);
    return errors;
}
