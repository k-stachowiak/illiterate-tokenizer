/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILTOK_BUFFERED_CHAR_STREAM_H
#define ILTOK_BUFFERED_CHAR_STREAM_H

#include "char_stream.h"

#include <stdbool.h>

// Public API
// ==========

struct TokBufferedCharacterStream {
    struct TokCharStream *cs;
    struct { char *data; int size, cap; } buffer;
    int buffer_end;
    size_t source_offset;
};

void tok_bcs_init(struct TokBufferedCharacterStream *bcs, struct TokCharStream *cs);
void tok_bcs_deinit(struct TokBufferedCharacterStream *bcs);

bool tok_bcs_has_next(struct TokBufferedCharacterStream *bcs);
int tok_bcs_peek_next(struct TokBufferedCharacterStream *bcs);
int tok_bcs_get_next(struct TokBufferedCharacterStream *bcs);

void tok_bcs_reset(struct TokBufferedCharacterStream *bcs);
void tok_bcs_reset_n(struct TokBufferedCharacterStream *bcs, int n);
void tok_bcs_commit(struct TokBufferedCharacterStream *bcs);

char *tok_bcs_copy_buffered(struct TokBufferedCharacterStream *bcs);

// Test code
// =========

int tok_bcs_test(void);

#endif
