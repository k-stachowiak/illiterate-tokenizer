/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "char_stream.h"
#include "internal.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// String based implementation
// ===========================

struct StringCharStream {
    char *first;
    char *last;
};

static void cs_string_deinit(void *state)
{
    (void)state;
}

static bool cs_string_has_next(void *state)
{
    struct StringCharStream *scs = (struct StringCharStream *)state;
    return scs->first != scs->last;
}

static int cs_string_peek_next(void *state)
{
    struct StringCharStream *scs = (struct StringCharStream *)state;
    if (cs_string_has_next(state)) {
        return *scs->first;
    } else {
        INTERNAL_ERROR("Illegal character stream read");
    }
}

static int cs_string_get_next(void *state)
{
    struct StringCharStream *scs = (struct StringCharStream *)state;
    if (cs_string_has_next(state)) {
        return *scs->first++;
    } else {
        INTERNAL_ERROR("Illegal character stream read");
    }
}

void tok_cs_init_string(struct TokCharStream *cs, char *string)
{
    struct StringCharStream *scs = malloc(sizeof(*scs));

    if (!scs) {
        INTERNAL_ERROR("Out of memory");
    }

    scs->first = string;
    scs->last = string + strlen(string);

    cs->state = scs;
    cs->deinit = cs_string_deinit;
    cs->has_next = cs_string_has_next;
    cs->peek_next = cs_string_peek_next;
    cs->get_next = cs_string_get_next;
}

// File based implementation
// =========================

struct FileCharStream {
    FILE *file;
    int temp;
};

static void cs_file_deinit(void *state)
{
    (void)state;
}

static bool cs_file_has_next(void *state)
{
    struct FileCharStream *fcs = (struct FileCharStream *)state;
    int c;

    if (fcs->temp == -1) {
        // a) No character in the buffer => load next one:
        c = fgetc(fcs->file);
        if (c == EOF) {
            // a)a) Can't load, we indeed have reached EOF:
            return false;
        } else {
            // a)b) Can load next, so we're not at EOF:
            fcs->temp = c;
            return true;
        }
    } else {
        // b) Character had already been loaded, so we have more:
        return true;
    }
}

static int cs_file_peek_next(void *state)
{
    struct FileCharStream *fcs = (struct FileCharStream *)state;
    int c;

    // 1. If we have not read a temporary character, do so:
    if (fcs->temp == -1) {
        c = getc(fcs->file);
        if (c == EOF) {
            INTERNAL_ERROR("Illegal character stream read");
        }
        fcs->temp = c;
    }

    // 2. Tell, what is the latest temporary character:
    return fcs->temp;
}

static int cs_file_get_next(void *state)
{
    struct FileCharStream *fcs = (struct FileCharStream *)state;
    int c;

    if (fcs->temp == -1) {
        // a) There is not temporary character => just read next character from
        //    file:
        c = getc(fcs->file);
        if (c == EOF) {
            INTERNAL_ERROR("Illegal character stream read");
        }
        return c;
    } else {
        // b) We already have a temporary character => return it and discard it
        //    locally.
        int result = fcs->temp;
        fcs->temp = -1;
        return result;
    }
}

void tok_cs_init_file(struct TokCharStream *cs, FILE *file)
{
    struct FileCharStream *fcs = malloc(sizeof(*fcs));

    if (!fcs) {
        INTERNAL_ERROR("Out of memory");
    }

    fcs->file = file;
    fcs->temp = -1;

    cs->state = fcs;
    cs->deinit = cs_file_deinit;
    cs->has_next = cs_file_has_next;
    cs->peek_next = cs_file_peek_next;
    cs->get_next = cs_file_get_next;
}

// Common implementation
// =====================

void tok_cs_deinit(struct TokCharStream *cs)
{
    cs->deinit(cs->state);
    free(cs->state);
}

bool tok_cs_has_next(struct TokCharStream *cs)
{
    return cs->has_next(cs->state);
}

int tok_cs_peek_next(struct TokCharStream *cs)
{
    return cs->peek_next(cs->state);
}

int tok_cs_get_next(struct TokCharStream *cs)
{
    return cs->get_next(cs->state);
}

void tok_cs_skip_if(struct TokCharStream *cs, int (*predicate)(int))
{
    while (tok_cs_has_next(cs) && predicate(tok_cs_peek_next(cs))) {
        (void)tok_cs_get_next(cs);
    }
}

// Test code
// =========

static int cs_test_generic(char *case_description, bool result)
{
    if (result) {
        return 0;
    } else {
        printf("Failed test case: %s\n", case_description);
        return 1;
    }
}

// Abstract test routines
// ----------------------

static int cs_test_empty(void (*initializer)(struct TokCharStream *, char *),
                         void (*deinitializer)(struct TokCharStream *))
{
    int errors = 0;
    struct TokCharStream cs;
    initializer(&cs, "");

    errors += cs_test_generic(
        "Empty has no next",
        tok_cs_has_next(&cs) == false);

    deinitializer(&cs);
    return errors;
}

static int cs_test_one_char_peek(struct TokCharStream *cs,
                                 char character)
{
    return
        cs_test_generic(
            "One char string has next",
            tok_cs_has_next(cs) == true) +
        cs_test_generic(
            "First peeked as expected",
            tok_cs_peek_next(cs) == character) +
        cs_test_generic(
            "First got as expected",
            tok_cs_get_next(cs) == character) +
        cs_test_generic(
            "One char string no longer has next after getting",
            tok_cs_has_next(cs) == false);
}

static int cs_test_one_char_no_peek(struct TokCharStream *cs,
                                    char character)
{
    return
        cs_test_generic(
            "One char string has next",
            tok_cs_has_next(cs) == true) +
        cs_test_generic(
            "First got as expected",
            tok_cs_get_next(cs) == character) +
        cs_test_generic(
            "One char string no longer has next after getting",
            tok_cs_has_next(cs) == false);
}

static int cs_test_one_char(void (*initializer)(struct TokCharStream *, char *),
                            void (*deinitializer)(struct TokCharStream *))
{
    int errors = 0;
    struct TokCharStream cs;

    // 1. Perform a test with character peeking with an arbitrary string:
    initializer(&cs, "1");
    errors += cs_test_one_char_peek(&cs, '1');
    deinitializer(&cs);

    // 2. Perform a test with no character peeking with another string:
    initializer(&cs, "2");
    errors += cs_test_one_char_no_peek(&cs, '2');
    deinitializer(&cs);

    return errors;
}

static int cs_test_two_char_peek(struct TokCharStream *cs,
                                 char character_1,
                                 char character_2)
{
    return
        cs_test_generic(
            "Two char string has next",
            tok_cs_has_next(cs) == true) +
        cs_test_generic(
            "First peeked as expected",
            tok_cs_peek_next(cs) == character_1) +
        cs_test_generic(
            "First got as expected",
            tok_cs_get_next(cs) == character_1) +
        cs_test_generic(
            "Two char string still has next after getting",
            tok_cs_has_next(cs) == true) +
        cs_test_generic(
            "Second peeked as expected",
            tok_cs_peek_next(cs) == character_2) +
        cs_test_generic(
            "Second got as expected",
            tok_cs_get_next(cs) == character_2) +
        cs_test_generic(
            "Two char string no longer has next after two gets",
            tok_cs_has_next(cs) == false);
}

static int cs_test_two_char_no_peek(struct TokCharStream *cs,
                                    char character_1,
                                    char character_2)
{
    return
        cs_test_generic(
            "Two char string has next",
            tok_cs_has_next(cs) == true) +
        cs_test_generic(
            "First got as expected",
            tok_cs_get_next(cs) == character_1) +
        cs_test_generic(
            "Two char string still has next after getting",
            tok_cs_has_next(cs) == true) +
        cs_test_generic(
            "Second got as expected",
            tok_cs_get_next(cs) == character_2) +
        cs_test_generic(
            "Two char string no longer has next after two gets",
            tok_cs_has_next(cs) == false);
}

static int cs_test_two_chars(void (*initializer)(struct TokCharStream *, char *),
                             void (*deinitializer)(struct TokCharStream *))
{
    int errors = 0;
    struct TokCharStream cs;

    // 1. Perform a test with character peeking with an arbitrary string:
    initializer(&cs, "12");
    errors += cs_test_two_char_peek(&cs, '1', '2');
    deinitializer(&cs);

    // 2. Perform a test with no character peeking with another string:
    initializer(&cs, "ab");
    errors += cs_test_two_char_no_peek(&cs, 'a', 'b');
    deinitializer(&cs);

    return errors;
}

static int cs_test_skip_ws_one_char(
        char *string,
        char character,
        void (*initializer)(struct TokCharStream *, char *),
        void (*deinitializer)(struct TokCharStream *))
{
    int errors = 0;
    struct TokCharStream cs;
    initializer(&cs, string);

    errors += cs_test_generic(
        "Has next before skipping leading WS",
        tok_cs_has_next(&cs) == true);

    tok_cs_skip_if(&cs, isspace);

    errors += cs_test_generic(
        "Has next after skipping leading WS",
        tok_cs_has_next(&cs) == true);
    errors += cs_test_generic(
        "First peeked as expected",
        tok_cs_peek_next(&cs) == character);
    errors += cs_test_generic(
        "First got as expected",
        tok_cs_get_next(&cs) == character);

    tok_cs_skip_if(&cs, isspace);

    errors += cs_test_generic(
        "No longer has next after skipping trailing WS",
        tok_cs_has_next(&cs) == false);

    deinitializer(&cs);

    return errors;
}

static int cs_test_skip_ws_two_chars(
        char *string,
        char character_1,
        char character_2,
        void (*initializer)(struct TokCharStream *, char *),
        void (*deinitializer)(struct TokCharStream *))
{
    int errors = 0;
    struct TokCharStream cs;
    initializer(&cs, string);

    errors += cs_test_generic(
        "Has next before skipping leading WS",
        tok_cs_has_next(&cs) == true);

    tok_cs_skip_if(&cs, isspace);

    errors += cs_test_generic(
        "Has next after skipping leading WS",
        tok_cs_has_next(&cs) == true);
    errors += cs_test_generic(
        "First peeked as expected",
        tok_cs_peek_next(&cs) == character_1);
    errors += cs_test_generic(
        "First got as expected",
        tok_cs_get_next(&cs) == character_1);
    errors += cs_test_generic(
        "Has next before skipping internal WS",
        tok_cs_has_next(&cs) == true);

    tok_cs_skip_if(&cs, isspace);

    errors += cs_test_generic(
        "Has next after skipping internal WS",
        tok_cs_has_next(&cs) == true);
    errors += cs_test_generic(
        "Second peeked as expected",
        tok_cs_peek_next(&cs) == character_2);
    errors += cs_test_generic(
        "Second got as expected",
        tok_cs_get_next(&cs) == character_2);

    tok_cs_skip_if(&cs, isspace);

    errors += cs_test_generic(
        "No longer has next after skipping trailing WS",
        tok_cs_has_next(&cs) == false);

    deinitializer(&cs);

    return errors;
}

static int cs_test_skip_if(void (*init)(struct TokCharStream *, char *),
                           void (*deinit)(struct TokCharStream *))
{
    return
        cs_test_skip_ws_one_char(        "x",         'x', init, deinit) +
        cs_test_skip_ws_one_char(" \t\n" "x",         'x', init, deinit) +
        cs_test_skip_ws_one_char(        "x" "\t \n", 'x', init, deinit) +
        cs_test_skip_ws_one_char("\t \n" "x" "\t\n",  'x', init, deinit) +

        cs_test_skip_ws_two_chars(        "x"         "y",        'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars("\t \n" "x"         "y",        'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars(        "x" " \n\t" "y",        'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars("\t \n" "x" " \n\t" "y",        'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars(        "x"         "y" "\t\n", 'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars("\t \n" "x"         "y" "\t\n", 'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars(        "x" " \n\t" "y" "\t\n", 'x', 'y', init, deinit) +
        cs_test_skip_ws_two_chars("\t \n" "x" " \n\t" "y" "\t\n", 'x', 'y', init, deinit);
}

static int cs_test_routine(void (*initializer)(struct TokCharStream *, char *),
                           void (*deinitializer)(struct TokCharStream *))
{
    return
        cs_test_empty(initializer, deinitializer) +
        cs_test_one_char(initializer, deinitializer) +
        cs_test_two_chars(initializer, deinitializer) +
        cs_test_skip_if(initializer, deinitializer);
}

// Implementation specific initializers and deinitializers
// -------------------------------------------------------

static void cs_test_string_initializer(struct TokCharStream *cs,
                                       char *content)
{
    tok_cs_init_string(cs, content);
}

static void cs_test_string_deinitializer(struct TokCharStream *cs)
{
    tok_cs_deinit(cs);
}

#define TMP_PATH "./tmp"

static void cs_test_file_initializer(struct TokCharStream *cs,
                                     char *content)
{
    FILE *file = fopen(TMP_PATH, "wb");
    if (!file) {
        INTERNAL_ERROR("Failed creating temporary test file");
    }
    fputs(content, file);
    fclose(file);
    file = fopen(TMP_PATH, "rb");
    tok_cs_init_file(cs, file);
}

static void cs_test_file_deinitializer(struct TokCharStream *cs)
{
    struct FileCharStream *fcs = (struct FileCharStream *)cs->state;
    fclose(fcs->file);
    if (remove(TMP_PATH) != 0) {
        INTERNAL_ERROR("Failed deleting temporary test file");
    }
    tok_cs_deinit(cs);
}

#undef TMP_PATH

// Main test entry point
// ---------------------

int tok_cs_test(void)
{
    return
        cs_test_routine(cs_test_string_initializer,
                        cs_test_string_deinitializer) +
        cs_test_routine(cs_test_file_initializer,
                        cs_test_file_deinitializer);
}
