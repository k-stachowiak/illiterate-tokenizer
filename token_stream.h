/*
 * Copyright (C) 2021 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Tokenizer.
 *
 * Language Programming Language is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Tokenizer is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILTOK_TOKEN_STREAM_H
#define ILTOK_TOKEN_STREAM_H

#include "buffered_char_stream.h"

// Public API
// ==========

// Token
// -----

struct Token {
    char *string;
    size_t source_offset;
};

void tok_init(struct Token *token, char *string, size_t source_offset);
void tok_init_invalid(struct Token *token);
void tok_deinit(struct Token *token);
bool tok_valid(struct Token *token);
struct Token tok_copy(struct Token *token);

// Token stream
// ------------

struct TokenStream {
    struct TokBufferedCharacterStream bcs;
    struct { char **data; int size, cap; } hard_tokens;
    struct { char ***data; int size, cap; } delimiter_pairs;
    struct Token last_token;

    struct { char **data; int size, cap; } error_stack;
};

void tok_ts_init(struct TokenStream *ts, struct TokCharStream *cs);
void tok_ts_deinit(struct TokenStream *ts);

bool tok_ts_add_hard_tokens(struct TokenStream *ts,
                            char **tokens,
                            int tokens_count);
bool tok_ts_add_delimiter_pairs(struct TokenStream *ts,
                                char *(*delimiters)[2],
                                int delimiters_count);

bool tok_ts_has_next(struct TokenStream *ts);
char *tok_ts_peek_next(struct TokenStream *ts);
size_t tok_ts_peek_offset(struct TokenStream *ts);
void tok_ts_drop_next(struct TokenStream *ts);
struct Token tok_ts_get_next(struct TokenStream *ts);

// Test code
// =========

int tok_ts_test(void);

#endif
